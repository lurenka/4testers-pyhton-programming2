friend_name = "Dominika"
friend_age = 29
years_of_friendship = 4
has_driven_licence = True
number_of_pets = 0

print("Name:", friend_name, sep = "\t")
print("Age:", friend_age, sep = "\n")
print("Years of friendship:", years_of_friendship)
print("Driving licence:", has_driven_licence)
print("Number of pets:", number_of_pets)

print("Name:", friend_name, "Age:", friend_age, "Years:", years_of_friendship, "Driving:", has_driven_licence, "Pets:", number_of_pets, sep = "\t")

name_surname = "Karolina Osinska"
email = "jioews"
phone_number = 123456789

print("Name:", name_surname, "\nEmail:", email, "\nPhone:", phone_number)

bio = "Name:" + name_surname + "\nEmail:" + email + "\nPhone:" + phone_number

bio_smarter = f"Name: {name_surname} \nEmail: + {email} + \nPhone: + {phone_number}"

bio_smarter_two = f"Name: {name_surname} \nEmail: + {email} + \nPhone: + {phone_number}"

