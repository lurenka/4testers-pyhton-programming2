shopping_lists = [ "oranges", "potatoes", "orange juice", "washing liquid" ]
shopping_lists.append("water")
number_of_items_to_buy = len(shopping_lists)
first_three_items = shopping_lists[0:3]

animal = {
    "name": "Burek",
    "age": 7,
    "male": True,
    "kind": "dog"
}
dog_age = animal["age"]
dog_name = animal["name"]
animal["age"] = 10
print(shopping_lists[0], shopping_lists[2])
print(shopping_lists)
print(shopping_lists[-1])
print(shopping_lists)
print(number_of_items_to_buy)
print(first_three_items)
print("Moj pies ma : ", dog_age, "lat")
print("Moj pies ma na imie", dog_name)
print(animal)


pet = [{
    "name": "Fred",
    "kind": "dog",
    "age": 1,
    "is_he_a_male": True,
    "weight": 16,
    "favorite_food": ["djir", "frneuiw"]
},
       {
    "name": "Jogi",
    "kind": "cat",
    "age": 3,
    "is_he_a_male": True,
    "weight": 6,
    "favorite_food": ["djirfvrdsr", "frneucfiw"]
}]

print(len(pet))

print(pet[0] ["name"])
print(pet[1]["age"])
